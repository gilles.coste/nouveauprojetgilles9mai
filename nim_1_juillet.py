

def nouveau_jeu():
    """
    retourne une situation initiale
    """
    nb_elements = 18 #nombre initial d'éléments (allumettes ou autres) à piocher
    return nb_elements

def afficher_le_nom_du_jeu():
    """
    affiche (print) le nom du jeu en capitales
    """
    print("Jeu de Nim (par Benoit Fourlegnie et Gilles Coste)")

def afficher(situation):
    """
    affiche (print) le plateau de jeu, éventuellement les scores, le nombre de vies, le nombre de coups déjà joués...
    """
    alignement="Il reste "+str(situation)+" élement(s) : " 
    element="@"
    for _ in range(situation):
        alignement=alignement+element+" " 
    print(alignement)
    
def imposer_le_joueur_actif_qui_commence():
    """
    affiche le joueur_actif actif en début de jeu :joueur1 ou joueur2
    """
    print("c'est le joueur 1 qui commence ! (ce n'est pas forcément un avantage ^ ^)")

def is_jeu_fini(situation, joueur_actif):
    """
    retourne un booléen pour déterminer si, dans la situation passée en paramètre, le jeu est fini
    """
    return situation<2

def coups_possibles(situation,joueur_actif):
    """
    retourne la liste des coups possible pour la situation actuelle
    """
    coups_permis = [2,3]
    return coups_permis

def peut_jouer(situation,joueur_actif):
    """
    retourne un booléen (True/False) si le joueur"joueur_actif" peut jouer dans la situation "situation"
    """
    return situation>=2

def coup_a_jouer_demande_au_joueur_humain(situation, joueur_actif):
    """
    invite (input) le joueur humain actif à saisir le coup qu'il souhaite
    jusqu'à ce que ce coup soit autorisé pour le joueur_actif dans cette situation
    retourne le coup valide à jouer
    """
    coup = int(input("Combien d'éléments souhaitez-vous retirer ? (2 ou 3)"))
    
    while coup not in coups_possibles(situation,joueur_actif):
        coup = int(input("Combien d'éléments souhaitez-vous retirer ? (2 ou 3)"))
    return coup
    
def nouvelle_situation(situation, joueur_actif, coup):
    """
    renvoie une nouvelle situation suite à un coup et rend actif le joueur autre que celui passé en paramètre 
    """
    situation=situation-coup_a_jouer_demande_au_joueur_humain(situation, joueur_actif)
    
    return situation

def afficher_l_issue_du_jeu(situation,joueur_actif):
    """
    affiche (print) l'issue du jeu (match nul ou nom du gagnant, message de conclusion)
    """
    if situation==1:
        print(str(joueur_actif)+" a perdu car c'est lui qui doit prendre le dernier élément")
    if situation==0:
        print(str(joueur_actif)+" a gagné car c'est l'autre joueur qui vient de prendre les derniers éléments")
    afficher_remerciements()
        
        
        
def afficher_remerciements():
    """
    droit d'auteur, de distribution...
    """
    print("Merci d'avoir jouer au jeu de Nim" )
