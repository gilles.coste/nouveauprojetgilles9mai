def estADN(sequence):
    '''
    vérifie si la chaîne de caractères
passée en paramètre ne contient aucun autre caractère que les quatre bases
A, C, G et T. Cette fonction renvoie la valeur True si tel est le cas,
et la valeur False dans le cas contraire. De plus, elle renvoie True
si la chaîne est vide
   '''
    res=True
    for i in range(0,len(sequence)):
        if sequence[i]!='A' and sequence[i]!='T' and sequence[i]!='G' and sequence[i]!='C':
            res=False
    return res

def genereADN(n):
    '''
    renvoie une séquence ADN
    générée aléatoirement et dont la taille est passée en paramètre.
    '''
    base=['A','T','G','C']
    sequence=""
    import random
    for i in range(n):
        sequence=sequence+random.choice(base)
    return sequence

def baseComplementaire(b,type):
    '''
    renvoie la base
complémentaire de la base passée en paramètre, selon le type de séquence
demandé en sortie qui peut être soit 'ADN', soit 'ARN'.
Les contraintes d'utilisation de cette fonction sont que le premier
paramètre est bien l'une des quatre bases de l'ADN (A, T, G ou C)
et le deuxième est la chaîne soit 'ADN', soit 'ARN'.
    '''
    baseCompl=""
    if b=="A":
        if type=="ADN":
            baseCompl="T"
        if type=="ARN":
            baseCompl="U"
    if b=="T":
        baseCompl="A"
    if b=="G":
        baseCompl="C"
    if b=="C":
        baseCompl="G"
    return baseCompl

def transcrit(sequence,debut,fin):
    '''
    qui renvoie l'ARN construit
    à partir de la sous-séquence d'ADN comprise entre les deux positions
    passées en paramètre, incluses. La première base de la séquence étant
    numérotée 1.
    Pour mémoire, cet ARN est la séquence complémentaire de la portion
    d'ADN transcrite.
    '''
    ARN=""
    debut=debut-1
    for i in range(debut,fin):
        ARN=ARN+baseComplementaire(sequence[i],"ARN")
    return ARN

def codeGenetique(codon):
    '''
    renvoie l'acide aminé
    (sous la forme du nom abrévié EN 1 lettre) correspondant au codon
    passé en paramètre, ou * pour les codons Stop.
    '''
    acideAmine="codon non identifié"
    if codon in ['UUU', 'UUC'] :
        acideAmine='F'
    if codon in ['UUA', 'UUG', 'CUU', 'CUC', 'CUA', 'CUG'] :
        acideAmine='L'
    if codon in ['AUU', 'AUC', 'AUA' ]:
        acideAmine='I'
    if codon in ['AUG' ]:
        acideAmine='M'
    if codon in ['GUU', 'GUC', 'GUA', 'GUG' ]:
        acideAmine='V'
    if codon in ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC'] :
        acideAmine='S'
    if codon in ['CCU', 'CCC', 'CCA', 'CCG' ]:
        acideAmine='P'
    if codon in ['ACU', 'ACC', 'ACA', 'ACG' ]:
        acideAmine='T'
    if codon in ['GCU', 'GCC', 'GCA', 'GCG' ]:
        acideAmine='A'
    if codon in ['UAU', 'UAC' ]:
        acideAmine='Y'
    if codon in ['UAA', 'UAG', 'UGA' ]:
        acideAmine='*'
    if codon in ['CAU', 'CAC' ]:
        acideAmine='H'
    if codon in ['CAA', 'CAG' ]:
        acideAmine='Q'
    if codon in ['AAU', 'AAC' ]:
        acideAmine='N'
    if codon in ['AAA', 'AAG' ]:
        acideAmine='K'
    if codon in ['GAU', 'GAC' ]:
        acideAmine='D'
    if codon in ['GAA', 'GAG' ]:
        acideAmine='E'
    if codon in ['UGU', 'UGC' ]:
        acideAmine='C'
    if codon in ['UGG' ]:
        acideAmine='W'
    if codon in ['CGU', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG' ]:
        acideAmine='R'
    if codon in ['GGU', 'GGC', 'GGA', 'GGG' ]:
        acideAmine='G'

    return acideAmine

def traduit(ARN):
    proteine=""
    for i in range(0,len(ARN),3):
        proteine=proteine+codeGenetique(ARN[i:i+3]) #i+3 exclus
    return proteine
    
def replique(ADN):
    '''
    construit la séquence ADN complémentaire et inversée de celle passée
    en paramètre. Pour cela, cette fonction fait appel à la fonction
    baseComplementaire
    '''
    if(estADN(ADN)):
        ADNrepliquee=""
        for i in range(0,len(ADN)):
            ADNrepliquee=baseComplementaire(ADN[i],"ADN")+ADNrepliquee
            return ADNrepliquee
    else:
        print("La séquence saisie n'est pas une chaine d'ADN")
        

    