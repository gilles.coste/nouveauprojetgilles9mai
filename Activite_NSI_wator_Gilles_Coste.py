'''
paramètres de la simulation (constantes)
'''
TEMPS_GESTATION_DES_THONS = 2
ENERGIE_DES_REQUINS = 3
DUREE_GESTATION_DES_REQUINS = 4
POURCENTAGE_POPULATION_REQUIN = 10 # en %
POURCENTAGE_POPULATION_THON = 30 # en %
HAUTEUR=20 #dimension de l'environnement
LARGEUR=30 #dimension de l'environnement
NB_DE_PAS=100000 #pour la simulation itérative


'''
modules nécessaires"
'''
from random import randrange
import numpy
from matplotlib import pylab

def grille_vide_generee(nb_colonnes,nb_lignes):
    '''
    génère une grille 2D rectangulaire dont
    les dimensions (int) sont passées en paramètres
    Cette grille sera considérée comme torique
    largeur L cases
    hauteur H cases
    L et H : int
    retourne une liste de listes (liste 2D)
    '''
    grille=[]
    for i in range (nb_lignes):
        grille.append([])
        for j in range(nb_colonnes):
            grille[i].append(0)
    #print(grille)
    return grille




mer=grille_vide_generee(LARGEUR,HAUTEUR) #création de l'environnement vide
grille_gestation=grille_vide_generee(LARGEUR,HAUTEUR)
grille_energie=grille_vide_generee(LARGEUR,HAUTEUR)


def peupler_environnement(pourcentageProie, pourcentagePredateur,gestationProie,gestationPredateur,energiePredateur):
    '''
    procédure pour peupler la mer en respectant le pourcentage
    en terme de population des 2 espèces
    Par codage, chaque case contient alors :
    0 : vide
    1 : proie (thon)
    2 : prédateur (requin)
    Cette procédure enregistre également les caractéristiques de ces individus dans des grilles déclarées en global
    '''
    #print(mer)
    
    nbPredateur = HAUTEUR*LARGEUR*pourcentagePredateur//100;
    #print("nb requin (2): "+str(nbPredateur))
    nbProie = HAUTEUR*LARGEUR*pourcentageProie//100;
    #print("nb thon (1): "+str(nbProie))
    
    
   
    
    for i in range(nbPredateur):
        ligne=randrange(0,HAUTEUR)
        colonne=randrange(0,LARGEUR)
        #print(str(ligne)+" "+str(colonne)+" "+str(mer[ligne][colonne]))

        while(mer[ligne][colonne]!=0):
            ligne=randrange(0,HAUTEUR)
            colonne=randrange(0,LARGEUR)
            
            #print(str(ligne)+" "+str(colonne)+" "+str(mer[ligne][colonne]))
        #print("nouveau requin")
        mer[ligne][colonne]=2
        grille_gestation[ligne][colonne]=gestationPredateur
        grille_energie[ligne][colonne]=energiePredateur
        #print(mer)
        
    for i in range(nbProie):
        ligne=randrange(0,HAUTEUR)
        colonne=randrange(0,LARGEUR)
        #print(str(ligne)+" "+str(colonne)+" "+str(mer[ligne][colonne]))

        while(mer[ligne][colonne]!=0):
            ligne=randrange(0,HAUTEUR)
            colonne=randrange(0,LARGEUR)
            #print(str(ligne)+" "+str(colonne)+" "+str(mer[ligne][colonne]))
        #print("nouveau thon")
        mer[ligne][colonne]=1
        grille_gestation[ligne][colonne]=gestationProie
        
    #print(mer)
    #print(grille_gestation)
    #print(grille_energie)
    

peupler_environnement(POURCENTAGE_POPULATION_THON, POURCENTAGE_POPULATION_REQUIN,TEMPS_GESTATION_DES_THONS,DUREE_GESTATION_DES_REQUINS,ENERGIE_DES_REQUINS)


def il_existe_case_vide_voisine(ligne,colonne):
    '''
    fonction qui retourne une variable booléenne (True/False)
    selon l'existence d'une case vide voisine
    de la case dont la position (ligne, colonne) est passée en paramètre
    ATTENTION : la grille est torique !!!
    ligne:int
    colonne:int
    '''
    resultat=False
    #on détaille les 4 voisins même si on aurait pu faire des tests combinés (or)
    if mer[(ligne+1)%HAUTEUR][colonne]==0 :#si la case d'en-dessous (S) est vide
        resultat=True
    if mer[(ligne-1+HAUTEUR)%HAUTEUR][colonne]==0 :#si la case d'au-dessus (N) est vide
        resultat=True
    if mer[ligne][(colonne+1)%LARGEUR]==0 :#si la case de droite (E) est vide
        resultat=True
    if mer[ligne][(colonne-1+LARGEUR)%LARGEUR]==0:#si la case de gauche (O) est vide
        resultat=True
        
    return resultat

def il_existe_case_voisine_avec_thon(ligne,colonne):
    '''
    fonction qui retourne une variable booléenne (True/False)
    selon l'existance d'une case vide voisine
    de la case dont la position (ligne, colonne) est passée en paramètre
    ATTENTION : la grille est torique !!!
    ligne:int
    colonne:int
    '''
    resultat=False
    #on détaille les 4 voisins même si on aurait pu faire des tests combinés (or)
    if mer[(ligne+1)%HAUTEUR][colonne]==1 :#si la case d'en-dessous (S) contient un thon
        resultat=True
    if mer[(ligne-1+HAUTEUR)%HAUTEUR][colonne]==1 :#si la case d'au-dessus (N) contient un thon
        resultat=True
    if mer[ligne][(colonne+1)%LARGEUR]==1 :#si la case de droite (E) contient un thon
        resultat=True
    if mer[ligne][(colonne-1+LARGEUR)%LARGEUR]==1:#si la case de gauche (O) contient un thon
        resultat=True
    
    return resultat

def liste_case_vide_voisine(ligne,colonne):
    '''
    fonction qui retourne la liste contenant les liste [ligne,colonne] des cases vides voisines
    de la case de position passée en paramètre
    ATTENTION : la grille est torique !!!
    ligne:int
    colonne:int
    '''
    liste=[]
    if mer[(ligne+1)%HAUTEUR][colonne]==0 :#si la case d'en-dessous (S) est vide
        liste.append([(ligne+1)%HAUTEUR,colonne])
    if mer[(ligne-1+HAUTEUR)%HAUTEUR][colonne]==0 :#si la case d'au-dessus (N) est vide
        liste.append([(ligne-1+HAUTEUR)%HAUTEUR,colonne])
    if mer[ligne][(colonne+1)%LARGEUR]==0 :#si la case de droite (E) est vide
        liste.append([ligne,(colonne+1)%LARGEUR])
    if mer[ligne][(colonne-1+LARGEUR)%LARGEUR]==0:#si la case de gauche (O) est vide
        liste.append([ligne,(colonne-1+LARGEUR)%LARGEUR])
        
    return liste

def liste_case_voisine_avec_thon(ligne,colonne):
    '''
    fonction qui retourne la liste contenant les liste [ligne,colonne] des cases vides voisines
    de la case de position passée en paramètre
    ATTENTION : la grille est torique !!!
    ligne:int
    colonne:int
    '''
    liste=[]
    if mer[(ligne+1)%HAUTEUR][colonne]==1 :#si la case d'en-dessous (S) contient un thon
        liste.append([(ligne+1)%HAUTEUR,colonne])
    if mer[(ligne-1+HAUTEUR)%HAUTEUR][colonne]==1 :#si la case d'au-dessus (N) contient un thon
        liste.append([(ligne-1+HAUTEUR)%HAUTEUR,colonne])
    if mer[ligne][(colonne+1)%LARGEUR]==1 :#si la case de droite (E) contient un thon
        liste.append([ligne,(colonne+1)%LARGEUR])
    if mer[ligne][(colonne-1+LARGEUR)%LARGEUR]==1:#si la case de gauche (O) contient un thon
        liste.append([ligne,(colonne-1+LARGEUR)%LARGEUR])
        
    return liste

def position_de_la_case_voisine_libre_de_destination(ligne,colonne):
    '''
    fonction qui retourne une liste de 2 éléments [ligne,colonne] désignant respectivement
    la ligne et la colonne d'une case vide (tirée au sort) voisine à la case dont la position
    est passée en paramètres
    ligne:int
    colonne:int
    
    '''
    destination=[ligne,colonne] #liste de 2 éléments dont le premier [0] indiquera la ligne, le 2ème [1] la colonne
    #destination=[]
    liste=liste_case_vide_voisine(ligne,colonne)
    alea = randrange(0,len(liste)) #tirage aléatoire de la case de destination
    destination=liste[alea]
    return destination
  
def position_de_la_case_voisine_avec_thon_de_destination(ligne,colonne):
    '''
    fonction qui retourne une liste de 2 éléments [ligne,colonne] désignant respectivement
    la ligne et la colonne d'une case contenant un thon (tirée au sort) voisine à la case dont la position
    est passée en paramètres
    ligne:int
    colonne:int
    
    '''
    destination=[ligne,colonne] #liste de 2 éléments dont le premier [0] indiquera la ligne, le 2ème [1] la colonne
    liste=liste_case_voisine_avec_thon(ligne,colonne)
    alea = randrange(0,len(liste)) #tirage aléatoire de la case de destination
    destination=liste[alea]
    return destination

def simuler_pas_suivant():
    '''
    procédure pour tirer aléatoirement une portion de l'environnement
    et simuler le comportement de l'individu qui si trouve
    '''
    ligne=randrange(0,HAUTEUR)
    colonne=randrange(0,LARGEUR)
    #print("case "+str(ligne)+" "+str(colonne))
    #print("contenu de la case "+str(mer[ligne][colonne]))
    
    if mer[ligne][colonne]==1:
        #print("c'est un thon")
        gerer_thon(ligne,colonne)
    if mer[ligne][colonne]==2:
        #print("c'est un requin")
        gerer_requin(ligne,colonne)


def gerer_thon(ligne,colonne):
    '''
    procédure simulant le comportement d'un thon situé dans une portion de
    l'environnement dont la position est passée en paramètres
    ligne:int
    colonne:int
    '''
    #print("c'est un thon")
    if il_existe_case_vide_voisine(ligne,colonne):#si il existe une case voisine vide
        #print("deplacement du thon")
        #liste=position_de_la_case_voisine_libre_de_destination(ligne,colonne)
        #ligne_destination=liste[0]
        ligne_destination=position_de_la_case_voisine_libre_de_destination(ligne,colonne)[0]
        #colonne_destination=liste[1]
        colonne_destination=position_de_la_case_voisine_libre_de_destination(ligne,colonne)[1]
        mer[ligne][colonne]=0 #le thon n'est plus ici
        mer[ligne_destination][colonne_destination]=1#le thon est maintenant là
        grille_gestation[ligne_destination][colonne_destination]=grille_gestation[ligne][colonne]-1 #la durée de gestion du thon diminue de 1
        grille_gestation[ligne][colonne]=0 #car le thon n'est plus ici
        if  grille_gestation[ligne_destination][colonne_destination]==0:
            #print("naissance d'un thon")
            mer[ligne][colonne]=1 #naissance d'un thon dans la case qui vient d'être quittée
            grille_gestation[ligne][colonne]=TEMPS_GESTATION_DES_THONS #intialisation du temps de gestion du thon venant de naître
            grille_gestation[ligne_destination][colonne_destination]=TEMPS_GESTATION_DES_THONS # réinitialisation du temps de gestation du thon qui a donné naissance
    else:#s'il n'existe pas de case voisine vide => pas de déplacement, pas de naissance
        #print("deplacement du thon impossible")
        grille_gestation[ligne][colonne]=grille_gestation[ligne][colonne]-1#la durée de gestion du thon diminue de 1
        if  grille_gestation[ligne][colonne]==0: #si ça devait être la naissance (qui est impossible)
            #print("naissance d'un thon impossible")
            grille_gestation[ligne][colonne]=TEMPS_GESTATION_DES_THONS# réinitialisation du temps de gestation du thon qui aurait du donner naissance
    
    
    
    
def gerer_requin(ligne,colonne):
    '''
    procédure simulant le comportement d'un requin situé dans une portion de
    l'environnement dont la position est passée en paramètres
    ligne:int
    colonne:int
    '''
    #print("c'est un requin")
    grille_energie[ligne][colonne]=grille_energie[ligne][colonne]-1#Le requin perd un point d'énergie
    #print(grille_energie[ligne][colonne])
    grille_gestation[ligne][colonne]=grille_gestation[ligne][colonne]-1#Le temps de gestation du requin est diminué de 1
    
    #deplacement :
    
    if il_existe_case_voisine_avec_thon(ligne,colonne):#si il existe une case voisine avec un thon
        #print("deplacement du requin en mangeant un thon")
        #liste=position_de_la_case_voisine_avec_thon_de_destination(ligne,colonne) A NE SURTOUT PAS FAIRE
        #ligne_destination=liste[0]
        #colonne_destination=liste[1]
        ligne_destination=position_de_la_case_voisine_avec_thon_de_destination(ligne,colonne)[0]
        colonne_destination=position_de_la_case_voisine_avec_thon_de_destination(ligne,colonne)[1]
        mer[ligne][colonne]=0 #le requin n'est plus ici
        grille_energie[ligne][colonne]=0 #comme le requin n'est plus ici, son énergie n'a plus lieu d'être
        mer[ligne_destination][colonne_destination]=2#le requin est maintenant là et le thon se fait manger=ecraser
        grille_gestation[ligne_destination][colonne_destination]=grille_gestation[ligne][colonne]#comme le requin s'est déplacé son temps de gestation aussi
        grille_gestation[ligne][colonne]=0 #comme le requin n'est plus ici, son temps de gestion n'a plus lieu d'être
        grille_energie[ligne_destination][colonne_destination]=ENERGIE_DES_REQUINS#Son niveau d'énergie est alors remis à sa valeur initiale car il a fait un bon repas
        le_requin_s_est_deplace=True
    else:#s'il n'y a pas de case voisine avec un thon (le requin ne mange pas)
        if il_existe_case_vide_voisine(ligne,colonne):#si il existe une case vide voisine
            #print("le requin se déplace sans manger")
            #liste=position_de_la_case_voisine_libre_de_destination(ligne,colonne) A NE SURTOUT PAS FAIRE
            #ligne_destination=liste[0]
            #colonne_destination=liste[1]
            ligne_destination=position_de_la_case_voisine_libre_de_destination(ligne,colonne)[0]
            colonne_destination=position_de_la_case_voisine_libre_de_destination(ligne,colonne)[1]
            mer[ligne][colonne]=0 #le requin n'est plus ici
            grille_energie[ligne_destination][colonne_destination]=grille_energie[ligne][colonne]#comme le requin s'est déplacé son énergie aussi
            grille_energie[ligne][colonne]=0 #comme le requin n'est plus ici, son énergie n'a plus lieu d'être
            mer[ligne_destination][colonne_destination]=2#le requin est maintenant là
            grille_gestation[ligne_destination][colonne_destination]=grille_gestation[ligne][colonne]#comme le requin s'est déplacé son temps de gestation aussi
            grille_gestation[ligne][colonne]=0 #comme le requin n'est plus ici, son temps de gestion n'a plus lieu d'être
            le_requin_s_est_deplace=True
            
        else:#si il n'existe pas de case vide voisine, le requin ne se déplace pas
            #print("deplacement du requin impossible par manque de place")
            le_requin_s_est_deplace=False
            
    if le_requin_s_est_deplace==True: #si le requin vient de se déplacer
        if grille_energie[ligne_destination][colonne_destination]==0:#si le requin n'a plus d'énergie (cela ne peut pas arriver s'il a mangé un thon)
            #print("un requin meurt d'epuisement")
            mer[ligne_destination][colonne_destination]=0 #le requin meurt et disparait
            grille_gestation[ligne_destination][colonne_destination]=0#le temps de gestation du requin n'a plus lieu d'être et devient nul
            #l'énergie du requin n'a plus lieu d'être et est déjà nulle
        else:#reproduction dans le cas où le requin qui a survecu vient de se déplacer
            if  grille_gestation[ligne_destination][colonne_destination]==0:
                #print("naissance d'un requin")
                mer[ligne][colonne]=2 #naissance d'un requin dans la case qui vient d'être quittée
                grille_gestation[ligne][colonne]=DUREE_GESTATION_DES_REQUINS #intialisation du temps de gestion du requin venant de naître
                grille_gestation[ligne_destination][colonne_destination]=DUREE_GESTATION_DES_REQUINS # réinitialisation du temps de gestation du requin qui a donné naissance
    else: #si le requin n'a pas pu se deplacer     
        #print("deplacement du requin impossible")
        if grille_energie[ligne][colonne]==0:#si le requin n'a plus d'énergie
            #print("un requin immobile meurt d'epuisement")
            mer[ligne][colonne]=0 #le requin meurt et disparait
            grille_gestation[ligne][colonne]=0#le temps de gestation du requin n'a plus lieu d'être et devient nul
            #l'énergie du requin n'a plus lieu d'être et est déjà nulle
        else:#si le requin immobile n'est pas mort, on s'intéresse à sa reproduction
            if  grille_gestation[ligne][colonne]==0:
                #print("naissance d'un requin impossible")#car le requin n'a pas pu se déplacer
                grille_gestation[ligne][colonne]=DUREE_GESTATION_DES_REQUINS # réinitialisation du temps de gestation du requin qui n'a pas pu donner naissance
                 
        
        
def lancer_simulation(nombre_iterations):
    '''
    procédure de lancement de la simulation composées d'un nombre défini de pas
    passé en paramètre
    nombre_iterations:int
    '''
    
    print("Simulation en cours : please wait...")#pour que l'utilisateur soit patient
    for i in range (nombre_iterations):
        simuler_pas_suivant()
        effectif_thon.append(nombre_individus_recenses(1))#on recompte les thons et on rajoute leur effectif à la liste
        effectif_requin.append(nombre_individus_recenses(2))#on recompte les requins et on rajoute leur effectif à la liste
        nb_de_pas_realise.append(i)#on crée l'axe des abscisses
    print("fin de simulation")
    tracer_graph()#on trace l'évolution des populations aux cours de la simulation.


    
def tracer_graph():
    '''
    procédure de tracés des courbes représentatives des populations des proies et des prédateurs
    au cours de la simulation
    '''
    pylab.title("WA-TOR : Simulation proie-prédateur par G.Coste")
    pylab.xlabel('nombre de pas')
    pylab.ylabel('Effectifs des populations')
    pylab.grid()
    pylab.plot(nb_de_pas_realise, effectif_thon,label="thons (proies)")
    pylab.plot(nb_de_pas_realise, effectif_requin,label="requins(prédateurs)")
    #pylab.legend()
    pylab.show()


def nombre_individus_recenses(type_d_individu):
    '''
    fonction qui renvoie le nombre d'individus
    de type proie ou prédateur (passé en paramètre)
    présents dans l'environnement
    type_d_individu : 1<=>proie , 2<=>predateur
    '''
    nombre=0
    #on parcourt tout l'environnement
    for i in range (HAUTEUR):
        for j in range (LARGEUR):
            if mer[i][j]==type_d_individu:#si on trouve un individu
                nombre=nombre+1 #on le recense
    return nombre

effectif_thon=[nombre_individus_recenses(1)]#on crée une liste contenant uniquement le nombre initial de thons
#print(str(effectif_thon[0]))
effectif_requin=[nombre_individus_recenses(2)]#on crée une liste contenant uniquement le nombre initial de requins
#print(str(effectif_requin[0]))
nb_de_pas_realise=[0]#on crée une liste contenant le nombre de pas de la simulation

lancer_simulation(NB_DE_PAS)