from collections import namedtuple

import Time
import tris_anonymes
from src import Competitor

def read_competitors(nomDuFichier):
    '''
    Réalisez une fonction nommée read_competitors paramétrée par le nom du
    fichier CSV contenant les données des inscrits, qui a pour résultat le dictionnaire de
    ces inscrits.
    '''
    inscrits ={}#dictionnaire des inscrits
    fichier = open(nomDuFichier, "r")
    msg=fichier.read() #contenu du fichier sous forme d'une chaine de caractères unique
    fichier.close()
    
    liste_inscrits=msg.split('\n')
    
    for dossart in range (1,len(liste_inscrits)-1):
        donnees_inscrits=liste_inscrits[dossart].split(';')
        #print(donnees_inscrits[0])
        #print(donnees_inscrits[1])
        #print(donnees_inscrits[2])
        #print(donnees_inscrits[3])
        #print(i)
        inscrits[dossart]=Competitor.create(donnees_inscrits[0],donnees_inscrits[1],donnees_inscrits[2],donnees_inscrits[3],dossart)
    return inscrits
    '''
    >>>read_competitors('data/small_inscrits.csv')
    >>>read_competitors('data/inscrits.csv')
    >>>read_competitors('data/small_inscrits.csv')['bib_num']
    >>>read_competitors('data/small_inscrits.csv')['first_name']
    >>>read_competitors('data/small_inscrits.csv')['last_name']
    >>>read_competitors('data/small_inscrits.csv')['sex']'
    >>>read_competitors('data/small_inscrits.csv')['birth_date']'
    >>>read_competitors('data/small_inscrits.csv')['performance']'
    
    >>>Competitor.get_firstname(read_competitors('data/small_inscrits.csv')[1])
    >>>Competitor.get_lastname(read_competitors('data/small_inscrits.csv')[1])
    >>>Competitor.get_birthdate(read_competitors('data/small_inscrits.csv')[1])
    >>>Competitor.get_bib_num(read_competitors('data/small_inscrits.csv')[1])
    >>>Competitor.get_sex (read_competitors('data/small_inscrits.csv')[1])
    >>>Competitor.get_performance(read_competitors('data/small_inscrits.csv')[1])
    >>>Competitor.to_string(read_competitors('data/small_inscrits.csv')[1])
    '''
    
    '''
MANIPULATION DU DICTIONNAIRE
    '''
    
'''
AFFICHAGE
'''
def afficher(dictionnaire):
    '''
    Réalisez une fonction qui prend en paramètre une collection (un "itérable", par exemple une liste)
    de données de type Competitor et affiche sur la sortie standard chacune de ces données à raison
    d'une par ligne (utilisez la fonction to_string de Competitor).
    Utilisez votre fonction pour afficher les compétiteurs contenus dans le dictionnaire produit par la fonction read_competitors.
    '''
    for inscrit in dictionnaire.values():#on parcourt les valeurs du dictionnaire
        print(Competitor.to_string(inscrit))
              
    '''
    >>>afficher(read_competitors('data/small_inscrits.csv'))
    
    >>>afficher(read_competitors('data/inscrits.csv'))
    '''
    
'''
SELECTIONS
'''
def select_competitor_by_bib(dictionnaire,numero_dossart):
    '''
    fonction  qui a pour résultat le compétiteur
    dont le numéro de dossard est passé en paramètre.
    '''
    try:
        return Competitor.to_string(dictionnaire[numero_dossart])
    except KeyError:
        return 'Aucun courreur inscrit ne porte ce numéro de dossart'

    
    '''
    >>>select_competitor_by_bib(read_competitors('data/inscrits.csv'),5)
    '''

def tester_fin_de_chaine(sufixe):
    '''
    procédure de test de fin de chaine
    '''
    liste=['toto','tutu','roro','lolo','iuy','iuro']
    for i in range(len(liste)):
        if liste[i].endswith(sufixe):
            print(liste[i])
    '''
    >>>tester_fin_de_chaine('o')
    >>>tester_fin_de_chaine('ro')
    '''
    
def select_competitor_by_birth_year(dictionnaire,annee_de_naissance):
    '''
    fonction dont le résultat
    est la liste des compétiteurs dont l'année de naissance correspond
    à une valeur passée en paramètre.
    '''
    
    liste=[]

    for inscrit in dictionnaire.keys():#on parcourt les clés du dictionnaire
    
        #print(Competitor.get_birthdate(dictionnaire[inscrit]))
        if(Competitor.get_birthdate(dictionnaire[inscrit]).endswith(str(annee_de_naissance))):
            liste.append(dictionnaire[inscrit])
        
    #return Competitor.get_birthdate(dictionnaire[1])
    return liste
    '''
    >>>select_competitor_by_birth_year(read_competitors('data/inscrits.csv'),1969)
    
    >>>Competitor.to_string(select_competitor_by_birth_year(read_competitors('data/inscrits.csv'),1969)[0])
    '''

def select_competitor_by_name(dictionnaire,fragment):
    '''
    fonction dont le résultat est la liste des compétiteurs
    dont le nom (last name) contient la chaîne de caractères
    passée en paramètre.
    '''
    
    liste=[]

    for inscrit in dictionnaire.keys():#on parcourt les clés du dictionnaire
    
        #print(Competitor.get_birthdate(dictionnaire[inscrit]))
        #if(Competitor.get_lastname(dictionnaire[inscrit]).endswith(str(fragment),0,len(Competitor.get_lastname(dictionnaire[inscrit])))):
        if fragment in Competitor.get_lastname(dictionnaire[inscrit]):
            liste.append(dictionnaire[inscrit])
        
    #return Competitor.get_birthdate(dictionnaire[1])
    return liste
    '''
    >>>select_competitor_by_name(read_competitors('data/inscrits.csv'),'ou')
    
    >>>Competitor.to_string(select_competitor_by_name(read_competitors('data/inscrits.csv'),'ou')[0])
    '''
    
'''
REPORT DES PERFORMANCES
'''

def read_performances(nomDuFichier):
    '''
    fonction nommée paramétrée par le nom
    du fichier CSV contenant les données des performances,
    qui renvoie le dictionnaire
    des performances contenues dans ce fichier.
    '''
    performances ={}#dictionnaire des performances
    fichier = open(nomDuFichier, "r")
    msg=fichier.read() #contenu du fichier sous forme d'une chaine de caractères unique
    fichier.close()
    
    liste_performances=msg.split('\n')
    
    
    for dossart in range (1,len(liste_performances)-1):
        donnees_perfcompare_lastnameormance=liste_performances[dossart].split(';')
        #print(donnees_performance[0])
        #print(donnees_performance[1])
        #print(donnees_performance[2])
        #print(donnees_performance[3])
        
        #on remplit le dictionnaire
        performances[int(donnees_performance[0])]=Time.create(int(donnees_performance[1]),int(donnees_performance[2]),int(donnees_performance[3]))
    
    
    return performances

    '''
    >>>read_performances('data/small_performances.csv')
    
    >>>read_performances('data/performances.csv')
    '''
    
def set_performances(dictionnaire_inscrits,dictionnaire_performances):
    '''
    fonction paramétrée par les deux
    dictionnaires qui modifie les fiches des compétiteurs en reportant leur
    performance. Cette fonction ne renvoie pas de valeur.
    '''
    for inscrit in dictionnaire_performances.keys():
        Competitor.set_performance (dictionnaire_inscrits[int(inscrit)], dictionnaire_performances[int(inscrit)])
    
    print(dictionnaire_inscrits)
    '''
    >>>set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv'))
    
    >>>afficher(read_competitors('data/small_inscrits.csv'))
    '''
    
'''
TRIS
'''
def sort_competitors_by_lastname(dictionnaire_inscrits):
    '''
    prend en paramètre un dictionnaire de compétiteurs,
    comme défini précédemment, et a pour résultat la liste des compétiteurs
    triée par ordre alphabétique  de leurs noms.
    '''
    liste=list(dictionnaire_inscrits.values())#transforme le dictionnaire en liste
    tris_anonymes.tri_2(liste, Competitor.compare_lastname)#tri la liste
    
    return liste
    '''
    >>>sort_competitors_by_lastname(read_competitors('data/small_inscrits.csv'))
    '''
 
def sort_competitors_by_performance(dictionnaire_inscrits):
    '''
    produit la liste des
    compétiteurs triée par ordre croissant des performances réalisées. Les
    compétiteurs sans résultat sont placés en fin de liste par ordre
    alphabétique.
    REMARQUE : les inscrits n'ayant pas de performance  doivent être placés en fin de classement
    penser à modifier dans le module competitor la fonction compare_performance
    '''
