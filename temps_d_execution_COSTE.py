#from listes import*
#from tris import*
from timeit import timeit

def test():
   return timeit(setup='from tris import tri_select; from listes import cree_liste_melangee ; from listes import cree_liste_croissante ; from listes import cree_liste_decroissante',
           stmt='tri_select(cree_liste_croissante(10))',
           number=100)

#def mesure_de_temps_pour_liste_de_longueur(longueur):
#    """
#    fonction Python à un paramètre, une longeur de
#    liste donnée, qui renvoie la mesure de temps pour des listes de
#    cette longueur.
#    """
#    return timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
#           stmt='tri_select(cree_liste_melangee('+str(longueur)+'))',
#           number=100)

def mesure_de_temps_pour_liste_de_longueur(type_de_tri, type_de_liste, longueur_max):
    """
    fonction Python à un paramètre, une longeur de
    liste donnée, qui renvoie la mesure de temps pour des listes de
    cette longueur.
    """
    res=[]
    for longueur in range (1,longueur_max):
        res.append(timeit(setup='from tris import '+str(type_de_tri)+'; from listes import cree_liste_melangee; from listes import cree_liste_croissante ; from listes import cree_liste_decroissante',
           stmt=str(type_de_tri)+'('+str(type_de_liste)+'('+str(longueur)+'))',
           number=100))
    return res
    """
    >>>mesure_de_temps_pour_liste_de_longueur('tri_select', 'cree_liste_melangee', 25)
    
    >>>mesure_de_temps_pour_liste_de_longueur('tri_select', 'cree_liste_croissante', 25)
    
    >>>mesure_de_temps_pour_liste_de_longueur('tri_select', 'cree_liste_decroissante', 25)
    """

import pylab
  
def produit_courbe_temps_d_execution(type_de_tri,type_de_liste,longueur_max):
    """
    >>> produit_courbe_temps_d_execution("tri_select",10)
    """
    NBRE_ESSAIS = 100
    temps_obtenus = mesure_de_temps_pour_liste_de_longueur(type_de_tri,longueur_max)
    longueur = [i for i in range (1,longueur_max)]
    pylab.title('Temps du tri par sélection (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.plot(longueur,temps_obtenus)
    pylab.show()


def mesure_de_temps_pour_liste_de_longueur_sort(type_de_liste,longueur_max):
    """
    fonction Python à un paramètre, une longeur de
    liste donnée, qui renvoie la mesure de temps pour des listes de
    cette longueur.
    """
    res=[]
    for longueur in range (1,longueur_max):
        res.append(timeit(setup='from listes import cree_liste_melangee; from listes import cree_liste_croissante ; from listes import cree_liste_decroissante',
                          stmt='('+str(type_de_liste)+'('+str(longueur)+').sort)',
           number=100))
    return res


def comparer_courbes_differents_tris(type_de_liste,longueur_max):
    '''
    procédure, permettant de tracer sur le même graphique les courbes de temps d'exécution
    des tri de listes aléatoires pour les 4 méthodes de tri abordé,
    mais également la méthode sort de Python.
    ayant pour paramètres :
    * le type de listes à trier :
        - mélangées : 'cree_liste_melangee'
        - déjà trié par ordre croissant : 'cree_liste_croissante'
        - déjà trié par ordre décroissant : 'cree_liste_decroissante'
    * la longueur maximale des listes à trier (int)
    '''
    print('Tris en cours... please wait...')
    courbe_tri_select = mesure_de_temps_pour_liste_de_longueur('tri_select',type_de_liste,longueur_max)
    #print(courbe_tri_select)
    courbe_tri_insert = mesure_de_temps_pour_liste_de_longueur('tri_insert',type_de_liste,longueur_max)
    #print(courbe_tri_insert)
    courbe_tri_rapid=mesure_de_temps_pour_liste_de_longueur('tri_rapide',type_de_liste,longueur_max)
    #print(courbe_tri_rapid)
    courbe_tri_fusion=mesure_de_temps_pour_liste_de_longueur('tri_fusion',type_de_liste,longueur_max)
    #print(courbe_tri_fusion)
    courbe_sort=mesure_de_temps_pour_liste_de_longueur_sort(type_de_liste,longueur_max)
    NBRE_ESSAIS = 100
    longueur = [i for i in range (1,longueur_max)]
    pylab.title('Comparaison des temps de tri selon le type de tri (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.plot(longueur,courbe_tri_select,label="tri selection")
    pylab.plot(longueur,courbe_tri_insert,label="tri insertion")
    pylab.plot(longueur,courbe_tri_rapid,label="tri rapide")
    pylab.plot(longueur,courbe_tri_fusion,label="tri fusion")
    pylab.plot(longueur,courbe_sort,label="sort")
    pylab.legend()
    pylab.show()

    '''
    >>>comparer_courbes_differents_tris('cree_liste_melangee',120)
    
    >>>comparer_courbes_differents_tris('cree_liste_croissante',120)
    
    >>>comparer_courbes_differents_tris('cree_liste_decroissante',120)
    '''
def comparer_sort_courbes_differents_types_de_listes(longueur_max):
    '''
    procédure, permettant de tracer sur le même graphique les courbes de temps d'exécution
    du tri sort (méthode python) des 3 types de listes (mélangées, déjà triées par ordre croissant, déjà triées par ordre décroissant) 
    ayant pour paramètres :
    * la longueur maximale des listes à trier (int)
    '''
    print('Tris en cours... please wait...')
    courbe_tri_liste_melangee = mesure_de_temps_pour_liste_de_longueur_sort('cree_liste_melangee',longueur_max)

    courbe_tri_liste_triee_croissant = mesure_de_temps_pour_liste_de_longueur_sort('cree_liste_croissante',longueur_max)

    courbe_tri_liste_triee_decroissant = mesure_de_temps_pour_liste_de_longueur_sort('cree_liste_decroissante',longueur_max)



    
    NBRE_ESSAIS = 100
    longueur = [i for i in range (1,longueur_max)]
    pylab.title('Comparaison des temps de tri sort selon le type de liste (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.plot(longueur,courbe_tri_liste_melangee,label="listes mélangées")
    pylab.plot(longueur,courbe_tri_liste_triee_croissant,label="listes déjà triées (croissant)")
    pylab.plot(longueur,courbe_tri_liste_triee_decroissant,label="listes déjà triées (décroissant)")
    pylab.legend()
    pylab.show()

    '''
    >>>comparer_sort_courbes_differents_types_de_listes(120)

    '''


def comparer_courbes_differents_types_de_listes(type_de_tri,longueur_max):
    '''
    procédure, permettant de tracer sur le même graphique les courbes de temps d'exécution
    des tri des 3 types de listes (mélangées, déjà triées par ordre croissant, déjà triées par ordre décroissant) 
    ayant pour paramètres :
    * le type de tri :
        - tri selection : 'tri_select'
        - tri insertion: 'tri_insert'
        - tri rapide : 'tri_rapide'
        - tri fusion : 'tri_fusion'
    * la longueur maximale des listes à trier (int)
    '''
    print('Tris en cours... please wait...')
    courbe_tri_liste_melangee = mesure_de_temps_pour_liste_de_longueur(type_de_tri,'cree_liste_melangee',longueur_max)
    #print(courbe_tri_select)
    courbe_tri_liste_triee_croissant = mesure_de_temps_pour_liste_de_longueur(type_de_tri,'cree_liste_croissante',longueur_max)
    #print(courbe_tri_insert)
    courbe_tri_liste_triee_decroissant=mesure_de_temps_pour_liste_de_longueur(type_de_tri,'cree_liste_decroissante',longueur_max)
    #print(courbe_tri_rapid)

    
    NBRE_ESSAIS = 100
    longueur = [i for i in range (1,longueur_max)]
    pylab.title('Comparaison des temps de tri selon le type de liste (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.plot(longueur,courbe_tri_liste_melangee,label="listes mélangées")
    pylab.plot(longueur,courbe_tri_liste_triee_croissant,label="listes déjà triées (croissant)")
    pylab.plot(longueur,courbe_tri_liste_triee_decroissant,label="listes déjà triées (décroissant)")
    pylab.legend()
    pylab.show()

    '''
    >>>comparer_courbes_differents_types_de_listes('tri_select',120)
    
    >>>comparer_courbes_differents_types_de_listes('tri_insert',120)
    
    >>>comparer_courbes_differents_types_de_listes('tri_rapide',120)
    
    >>>comparer_courbes_differents_types_de_listes('tri_fusion',120)
    '''