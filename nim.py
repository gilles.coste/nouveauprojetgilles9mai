

def nouveau_jeu():
    """
    retourne une situation initiale
    """

def afficher_le_nom_du_jeu():
    """
    affiche (print) le nom du jeu en capitales
    """
    

def afficher(situation):
    """
    affiche (print) le plateau de jeu, éventuellement les scores, le nombre de vies, le nombre de coups déjà joués...
    """
 
def imposer_le_joueur_actif_qui_commence():
    """
    affiche le joueur_actif actif en début de jeu :joueur1 ou joueur2
    """

def is_jeu_fini(situation, joueur_actif):
    """
    retourne un booléen pour déterminer si, dans la situation passée en paramètre, le jeu est fini
    """
    
def coups_possibles(situation,joueur_actif):
    """
    retourne la liste des coups possible pour la situation actuelle
    """

def peut_jouer(situation,joueur_actif):
    """
    retourne un booléen (True/False) si le joueur"joueur_actif" peut jouer dans la situation "situation"
    """

def coup_a_jouer_demande_au_joueur_humain(situation, joueur_actif):
    """
    invite (input) le joueur humain actif à saisir le coup qu'il souhaite
    jusqu'à ce que ce coup soit autorisé pour le joueur_actif dans cette situation
    retourne le coup valide à jouer
    """
    
def nouvelle_situation(situation, joueur_actif, coup):
    """
    renvoie une nouvelle situation suite à un coup et rend actif le joueur autre que celui passé en paramètre 
    """


def afficher_l_issue_du_jeu(situation,joueur_actif):
    """
    affiche (print) l'issue du jeu (match nul ou nom du gagnant, message de conclusion)
    """
def afficher_remerciements():
    """
    droit d'auteur, de distribution...
    """
    