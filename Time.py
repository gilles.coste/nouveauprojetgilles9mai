from collections import namedtuple
from src import Competitor

Time = namedtuple('Time', ['hours', 'minutes', 'seconds']) #type Time

def create(heures,minutes,secondes):
    '''
    une fonction create à trois arguments, permettant de créer une donnée de type Time,
    dont le résultat est la donnée Time créée (on peut envisager un contrôle de validité
    des valeurs des paramètres pour ce constructeur)
    '''
    return Time(heures,minutes,secondes)
    
    '''
    >>>create(2,3,30)
    '''
    
def compare(Time1,Time2):
    '''
    fonction qui définit une relation d'ordre sur les données de type Time.
    De manière classique le résultat de cette fonction, à deux paramètres de type Time,
    est négatif si son premier paramètre est inférieur au second, positif s'il lui est
    plus grand et nul quand ils sont égaux.
    '''
    difference=(Time1.hours*3600+Time1.minutes*60+Time1.seconds)-(Time2.hours*3600+Time2.minutes*60+Time2.seconds)
    return difference
    '''
    >>>compare(create(0,30,50),create(1,10,35))
    '''

def to_string(Time1):
    '''
    une fonction qui a pour résultat une représentation sous la forme
    d'une chaîne de caractères de son paramètre de type Time.
    '''
    chaine= str(Time1.hours) +' h '+ str(Time1.minutes) +' min '+ str(Time1.seconds) +' s'
    return chaine
    '''
    >>>to_string(create(1,20,30))
    '''
