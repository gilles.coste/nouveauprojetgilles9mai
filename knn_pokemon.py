#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`knn_pokemon` module

:author: `FIL - Département Informatique - Université de Lille <http://portail.fil.univ-lille1.fr>`_

:date: Juillet, 2019

Module for knn algorithm.


"""

from pokemon import *

def nearest_neighbors(pokemon, train_data, k,
                      mini_maxi_values,
                      distance):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param mini_maxi_values: (dict) dictionnary that contains for every numeric attribute the couple of minimum and maximum values
    :param distance: (function) distance function used
    :return: (list of Pokemon) list of k nearest pokemon

    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> m_m_values = min_max_values(train)
    >>> len(nearest_neighbors(test[0], train, 4, m_m_values))
    4
    """
    liste_des_distances=[]
    for autrepoke in train_data:
        liste_des_distances.append((distance(pokemon, autrepoke, mini_maxi_values),autrepoke))
    liste_des_distances.sort()
    
    liste_des_k_plus_proches_voisins=[]
    for element in liste_des_distances:
        #print(element[1])
        liste_des_k_plus_proches_voisins.append(element[1])
    liste_des_k_plus_proches_voisins=liste_des_k_plus_proches_voisins[0:k]    
    return liste_des_k_plus_proches_voisins
"""
l = read_pokemon_csv()
#train, test = split_pokemons(l)
m_m_values = min_max_values(l)
print(nearest_neighbors(l[0], l, 7, m_m_values,pokemon_manhattan_distance))
print(nearest_neighbors(l[0], l, 7, m_m_values,pokemon_euclidian_distance))
"""
"""
l = read_pokemon_csv()
train, test = split_pokemons(l)
m_m_values = min_max_values(train)
print(nearest_neighbors(test[0], train, 7, m_m_values,pokemon_manhattan_distance))
print(nearest_neighbors(test[0], train, 7, m_m_values,pokemon_euclidian_distance))
"""

def knn(pokemon, train_data, k,
        mini_maxi_values,
        distance ,
        prop = 'is_legendary'):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param mini_maxi_values: (dict) dictionnary that contains for every numeric attribute the couple of minimum and maximum values
    :param distance: (function) distance function used
    :param prop: (str) name of the class to be predicted
    :return: (any) the class mostly present in pokemon neighborhood

    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> m_m_values = min_max_values(train)
    >>> knn(test[0], train, 3, m_m_values) in { True, False }
    True
    """
    nb_pokemon_legendaire=0
    for element in nearest_neighbors(pokemon, train_data, k, mini_maxi_values, distance):
        #print(element[18]) #l'indice 18 correspond à is_legendary
        if element[18]==True:
           nb_pokemon_legendaire=nb_pokemon_legendaire+1
    proportion=nb_pokemon_legendaire/k
    #print("coucou") 
    return proportion>0.5

    
"""
l = read_pokemon_csv()
train, test = split_pokemons(l)
m_m_values = min_max_values(train)
print(knn(test[0], train, 20, m_m_values)) #in { True, False } 
"""
"""
l = read_pokemon_csv()
m_m_values = min_max_values(l)
print("avec la distance euclidienne")
print(knn(l[18], l, 3, m_m_values,pokemon_euclidian_distance)) #  car l[18] est légendaire
print("avec la distance de manhattan")
print(knn(l[18], l, 3, m_m_values,pokemon_manhattan_distance)) #  car l[18] est légendaire 
"""
train = read_pokemon_csv('pokemon_train.csv')
test = read_pokemon_csv('pokemon_test.csv')
suspect1 = read_pokemon_csv('pokemon_suspect1.csv')
suspect2 = read_pokemon_csv('pokemon_suspect2.csv')


def knn_data(test_data, train_data, k,
             distance = pokemon_manhattan_distance,
             prop = 'is_legendary'):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param distance: (function) distance function used
    :return: (float) proportion of well classified data 
    
    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> knn_data(test, train, 4) > 0.6
    True
    """
    res = 0
    mini_maxi_values = min_max_values(train_data)
    for p in test_data:
        cl = knn(p, train_data, k, mini_maxi_values, distance, prop)
        if cl == p._asdict()[prop]:
            res += 1
    return res / len(test_data)



"""
if __name__ == '__main__':
    import doctest
    doctest.testmod()
"""