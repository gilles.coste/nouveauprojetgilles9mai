

def nouveau_jeu():
    """
    retourne une situation initiale
    """
    grille = []
    nb_lignes = 6
    nb_colonnes = 7
    for i in range (nb_lignes): # 6 lignes 
        grille.append([])
        for j in range (nb_colonnes): # 7 colonnes
            grille[i].append(0) #on remplit la grille avec des 0 (vide)
    return grille

def afficher_le_nom_du_jeu():
    """
    affiche (print) le nom du jeu en capitales
    """
    print("Puissance 4 (par Benoit Fourlegnie et Gilles Coste)")

def afficher(situation):
    """
    affiche (print) le plateau de jeu, éventuellement les scores, le nombre de vies, le nombre de coups déjà joués...
    """
    
    affichage_grille="+"
    for i in range (len(situation[0])):
        affichage_grille = affichage_grille + "---+"
    affichage_grille = affichage_grille + "\n"
    ligne = "| " #debut du ligne
    for i in range (1,len(situation[0])+1):
        ligne = ligne + str(i) +" | "
    affichage_grille=affichage_grille + ligne
    for i in range (len(situation)):
        ligne = "| " #debut du ligne
        for j in range(len(situation[0])):
            if situation[i][j]==0:#si la case est vide
                pion = " " #on n'affiche rien (un espace)
            else :
                pion = situation[i][j] #sinon on affiche le pion (x ou o)
            ligne = ligne + pion + " | "
        ligne = ligne +"\n"#fin de ligne avec retour chariot
        affichage_grille = ligne + affichage_grille #on rajoute par concaténation la ligne au dessus
    print(affichage_grille)
    
    """
    >>> afficher([["x", "x", "o", "x", 0, "x", "o"], ["x", "o", "x", "o", 0, "x", 0], ["x", "o", "x", 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["x", 0, 0, 0, 0, 0, 0]])
    
    """
    
def imposer_le_joueur_actif_qui_commence():
    """
    affiche le joueur_actif actif en début de jeu :joueur1 ou joueur2
    """
    print("c'est le joueur 1 qui commence ! (ce n'est pas forcément un avantage ^ ^)")

def is_jeu_fini(situation, joueur_actif):
    """
    retourne un booléen pour déterminer si, dans la situation passée en paramètre, le jeu est fini
    """
    #s'il n'y a plus de coup possible ou s'il y a un alignement, la partie est terminée
    return (len(coups_possibles(situation,joueur_actif))==0 or il_existe_un_alignement)

def il_existe_un_alignement(situation, joueur_actif):
    alignement=False
    #test alignement horizontal:
    for ligne in range(0,len(situation)):
        for colonne in range (0,len(situation[0])-3):
            if situation[ligne][colonne]!=0 and situation[ligne][colonne]==situation[ligne][colonne+1] and situation[ligne][colonne]==situation[ligne][colonne+2] and situation[ligne][colonne]==situation[ligne][colonne+3]:
                return True
    
    #test alignement vertical:
    for colonne in range(0,len(situation[0])):
        for ligne in range (0,len(situation)-3):
            if situation[ligne][colonne]!=0 and situation[ligne][colonne]==situation[ligne+1][colonne] and situation[ligne][colonne]==situation[ligne+2][colonne] and situation[ligne][colonne]==situation[ligne+3][colonne]:
                return True
    
    #test alignement diagonal montant:
    for ligne in range(0,len(situation)-3):
        for colonne in range (0,len(situation[0])-3):
            if situation[ligne][colonne]!=0 and situation[ligne][colonne]==situation[ligne+1][colonne+1] and situation[ligne][colonne]==situation[ligne+2][colonne+2] and situation[ligne][colonne]==situation[ligne+3][colonne+3]:
                return True    
    
    #test alignement diagonal descendant:
    for ligne in range(0,len(situation)-3):
        for colonne in range (3,len(situation[0])):
            if situation[ligne][colonne]!=0 and situation[ligne][colonne]==situation[ligne+1][colonne-1] and situation[ligne][colonne]==situation[ligne+2][colonne-2] and situation[ligne][colonne]==situation[ligne+3][colonne-3]:
                return True
            
    #print("fin")
    #si pas d'alignement détecté
    return False
    """
    >>> il_existe_un_alignement([["x", "x", "o", "x", 0, "x", "o"], ["x", "o", "x", "o", 0, "x", 0], ["x", "o", "x", 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["x", "x", "o", "x", "o", "o", "x"]],"joueur1")
    False
    
    >>> il_existe_un_alignement([["x", "x", "o", "x", 0, "x", "o"], ["x", "o", "x", "o", 0, "x", 0], ["x", "o", "x", 0, 0, 0, 0], ["o", "x", "x", 0, 0, 0, 0], ["o", "x", "x", 0, 0, 0, 0], ["x", "x", "o", "x", "o", "o", "x"]],"joueur1")
    True
    
    
    """

def coups_possibles(situation,joueur_actif):
    """
    retourne la liste des coups possible pour la situation actuelle
    """
    coups_permis = [] #liste des indices des colonnes dans lesquelles on peut jouer en y déposant un jeton
    indice_ligne_du_haut = len(situation)-1 # indice de la ligne du haut de la grille de jeu
    for indice_colonne in range(len(situation[0])):#on examine chaque colonne pour savoir si on peut y déposer un pion
        if situation[indice_ligne_du_haut][indice_colonne]==0:# si le haut de la colonne est libre
            coups_permis.append(indice_colonne) #alors il est permis de jouer dans cette colonne
    return coups_permis
    
    """
    >>>coups_possibles([["x", "x", "o", "x", 0, "x", "o"], ["x", "o", "x", "o", 0, "x", 0], ["x", "o", "x", 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["x", 0, "o", "x", 0, "o", 0]],"joueur1")
    [1, 4, 6]
    """

def peut_jouer(situation,joueur_actif):
    """
    retourne un booléen (True/False) si le joueur"joueur_actif" peut jouer dans la situation "situation"
    """
    return len(coups_possibles(situation,joueur_actif))!=0
    """
    >>>peut_jouer([["x", "x", "o", "x", 0, "x", "o"], ["x", "o", "x", "o", 0, "x", 0], ["x", "o", "x", 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["x", 0, "o", "x", 0, "o", 0]],"joueur1")
    True
    
    >>>peut_jouer([["x", "x", "o", "x", 0, "x", "o"], ["x", "o", "x", "o", 0, "x", 0], ["x", "o", "x", 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["o", "x", 0, 0, 0, 0, 0], ["x", "x", "o", "x", "o", "o", "x"]],"joueur1")
    False
    """

def coup_a_jouer_demande_au_joueur_humain(situation, joueur_actif):
    """
    invite (input) le joueur humain actif à saisir le coup qu'il souhaite
    jusqu'à ce que ce coup soit autorisé pour le joueur_actif dans cette situation
    retourne le coup valide à jouer
    """
    
    
def nouvelle_situation(situation, joueur_actif, coup):
    """
    renvoie une nouvelle situation suite à un coup et rend actif le joueur autre que celui passé en paramètre 
    """
    

def afficher_l_issue_du_jeu(situation,joueur_actif):
    """
    affiche (print) l'issue du jeu (match nul ou nom du gagnant, message de conclusion)
    """
    
        
        
        
def afficher_remerciements():
    """
    droit d'auteur, de distribution...
    """
    print('Merci d\'avoir jouer au "Puissance 4"' )
